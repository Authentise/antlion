Antlion
-------

Here's the problem. We need to use docker swarm for various reasons. Docker swarm is rife with problems with DNS, containers that fail to fully and properly start, networks that drop packets, etc. In most situations the strategy to deal with them is to restart the container.

That's a bummer.

So what antlion does is it monitors your swarm's health on each node and kills containers that aren't up to snuff. Then we expect the swarm to try again and bring them up
