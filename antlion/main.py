import asyncio
import logging
import time

import drophi.client

LOGGER = logging.getLogger(__name__)

async def start():
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('drophi').setLevel(logging.INFO)
    client = drophi.client.Client()
    try:
        while True:
            containers, tasks, services = await asyncio.gather(
                client.ps(),
                client.task_ls(),
                client.service_ls(),
            )
            await _kill_starting_containers(client, containers, tasks, services)
            await asyncio.sleep(60)
    finally:
        await client.close()

async def _kill_starting_containers(client, containers, tasks, services, use_kill=False):
    "Find containers that have been starting for too long. IE, longer than 10 minutes"
    LOGGER.debug("%s containers that are still starting", "Killing" if use_kill else "Stopping")
    TOO_OLD = 60 * 10
    starting_containers = [c for c in containers if '(health: starting)' in c['Status']]
    if not starting_containers:
        return
    old_containers = [c for c in starting_containers if time.time() - c['Created'] > TOO_OLD]
    if not old_containers:
        return
    LOGGER.debug("Found %d old containers that are still starting", len(old_containers))
    try:
        await asyncio.wait_for(
            asyncio.gather(
                *[_stop_container(client, container, tasks, services) for container in old_containers]
            ), timeout=30)
        LOGGER.info("Stopped containers %s for taking more than %s seconds to finish starting",
            ", ".join([c['Id'] for c in old_containers]),
            TOO_OLD)
    except asyncio.TimeoutError:
        LOGGER.info("Timed out waiting for containers to stop")

async def _stop_container(client, container, tasks, services):
    "Forcibly stop the container, first by scaling the service, then by stop/kill"
    task = _get_task_for_container(container, tasks)
    if not task:
        try:
            await asyncio.wait_for(
                client.container_kill(container['Id'])
            , timeout=20)
        except asyncio.TimeoutError:
            LOGGER.error("Timed out waiting for container %s to be killed", container['Id'])
            return
    service = _get_service_for_task(task, services)
    desired_replicas = service.replicas
    LOGGER.debug("Scaling service down to 0, then back to %d", desired_replicas)
    service.replicas = 0
    await client.service_update(service.id, service.version, service.to_payload())
    while (await _service_has_running_tasks(client, service)):
        await asyncio.sleep(3)
    new_services = await client.service_ls()
    new_service = _get_service_for_task(task, new_services)
    new_service.replicas = desired_replicas
    await client.service_update(new_service.id, new_service.version, new_service.to_payload())
    LOGGER.info("Scaled service %s down to 0, then back up to %d", new_service, desired_replicas)

async def _service_has_running_tasks(client, service):
    tasks = await client.task_ls()
    for task in tasks:
        if service.id == task['ServiceID'] and task['Status']['State'] != "shutdown":
            return True

def _get_service_for_task(task, services):
    "Get the service that matches the task"
    for service in services:
        if service.id == task['ServiceID']:
            return service

def _get_task_for_container(container, tasks):
    "Get the task that matches the container"
    for task in tasks:
        if task['Status']['ContainerStatus'].get('ContainerID') == container['Id']:
            return task
