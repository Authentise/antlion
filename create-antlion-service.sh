#!/bin/bash
docker service create \
    --name="antlion" \
    --replicas 1 \
    --mount type=bind,source=/home/eliribble/src/antlion/,destination=/src \
    --mount type=bind,source=/var/run/docker.sock,destination=/var/run/docker.sock \
    --constraint 'node.labels.antlion==true' \
    --entrypoint /bin/sh \
    authentise/maxillo:1.51 -c 'sleep 86400'
